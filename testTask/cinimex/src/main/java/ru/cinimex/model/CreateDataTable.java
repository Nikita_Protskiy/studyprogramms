package ru.cinimex.model;


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class CreateDataTable {
    private static String sqlCheck;
    private SimpleDriverDataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public void connectionToDB(){
        dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(org.h2.Driver.class);
        dataSource.setUsername("sa");
        dataSource.setUrl("jdbc:h2:mem:test");
        dataSource.setPassword("");
        jdbcTemplate = new JdbcTemplate(dataSource);
        System.out.println("Try to connect to database...");
    }

    public void insertSql(){
        try {
            connectionToDB();
            System.out.println("creating data table");
            jdbcTemplate.execute("DROP TABLE if EXISTS test;");
            jdbcTemplate.execute("CREATE TABLE test(ID INT PRIMARY KEY, TEXT VARCHAR(255), INSERT_DATE DATE);");
            jdbcTemplate.update("INSERT INTO test VALUES(1, 'Hello', '2016-01-01');");
            jdbcTemplate.update("INSERT INTO test VALUES(2, 'World', '2016-01-02');");
            sqlCheck = "db updated";
        }catch (Exception e){
            sqlCheck = "Have error " + e;
            System.err.println(sqlCheck);
        }
    }


    public Collection<Item> getAllItems(){
        insertSql();
        Collection<Item> items = new ArrayList<Item>();
        try {
            Statement statement = jdbcTemplate.getDataSource().getConnection().createStatement();
            ResultSet rs = statement.executeQuery("select * from test;");
            while (rs.next()){
                Item item = new Item();
                item.setId(rs.getInt("ID"));
                item.setText(rs.getString("TEXT"));
                item.setData(rs.getString("INSERT_DATE"));
                items.add(item);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return items;
    }
}
