package ru.cinimex.model;


public class Item {
    private int id;
    private String data;
    private String text;

    public String getInfo() {
        return (id + " " + text + " " + data);
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
