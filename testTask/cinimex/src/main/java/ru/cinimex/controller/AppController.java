package ru.cinimex.controller;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.cinimex.model.CreateDataTable;


@Controller
public class AppController {

    @RequestMapping("/")
    public String create(Model model){
        model.addAttribute("smth","Info from Table");
        return "create";
    }
}
