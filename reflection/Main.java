package reflection;


import lesson7.Parcer;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws NoSuchFieldException, IOException {
        Test test = new Test();
        Parcer parcer = new Parcer();
        MetaClass mc = new MetaClass(test);
        mc = new MetaClass(parcer);
    }
}
