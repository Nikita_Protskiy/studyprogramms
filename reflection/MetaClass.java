package reflection;

import java.io.*;
import java.lang.reflect.*;

/**
 * Created by Никита on 03.05.2016.
 */
public class MetaClass {
    private BufferedWriter bufferedWritter;
    private String filePath;
    private Object object;
    private static int number = 0;

    MetaClass(Object object) throws NoSuchFieldException, IOException {
        this.object = object;
        Class clazz = object.getClass();
        this.filePath = "H:\\books\\"+clazz.getName()+"_"+String.valueOf(number++)+"_"+".txt";

        bufferedWritter =  returnBufferedWritter();

        bufferedWritter.write("пакет = " + clazz.getPackage());
        bufferedWritter.newLine();
        bufferedWritter.write("\nкласс = " + clazz.getSimpleName());

        if (Modifier.isPublic(clazz.getModifiers())) {
            bufferedWritter.write(" открыт всем");
        }
        if (Modifier.isPrivate(clazz.getModifiers())) {
            bufferedWritter.write("скрыт от всех");
        }
        if (Modifier.isProtected(clazz.getModifiers())) {
            bufferedWritter.write("наследуемый");
        }
        bufferedWritter.newLine();
        bufferedWritter.write("\nнаследник =  " + clazz.getSuperclass());
        bufferedWritter.newLine();
        bufferedWritter.write("\nинтерфейсы:");
        for (Class aClass : clazz.getInterfaces()) {
            bufferedWritter.newLine();
            bufferedWritter.write("\n" + aClass.toString());
        }
        bufferedWritter.newLine();
        bufferedWritter.write("\nконструкторы:");
        for (Constructor constructor : clazz.getDeclaredConstructors()) {
            bufferedWritter.newLine();
            bufferedWritter.write("\n" + constructor.getName());
            if (Modifier.isPublic(constructor.getModifiers())){
                bufferedWritter.write(" открыт всем");
            }
            if (Modifier.isPrivate(constructor.getModifiers())){
                bufferedWritter.write(" скрыт от всех");
            }
            if (Modifier.isProtected(constructor.getModifiers())){
                bufferedWritter.write(" наследуемый");
            }

            if (constructor.getParameters().length != 0) {
                bufferedWritter.write(" параметры = ");
                for (Parameter parameter : constructor.getParameters()) {
                    bufferedWritter.write(" " + parameter.getType().getName() + ",");
                }
            }else {
                bufferedWritter.write(" по умолчанию");
            }

        }
        bufferedWritter.newLine();
        bufferedWritter.write("\nполя :\n");
        for (Field field : clazz.getDeclaredFields()) {
            bufferedWritter.newLine();
            bufferedWritter.write(field.getName() + " ,");
            if (Modifier.isPublic(field.getModifiers())){
                bufferedWritter.write("открыт всем, ");

            }
            if (Modifier.isPrivate(field.getModifiers())){
                bufferedWritter.write("скрыт от всех, ");
            }
            if (Modifier.isProtected(field.getModifiers())){
                bufferedWritter.write("наследуемый, ");
            }
            if (Modifier.isStatic(field.getModifiers())){
                bufferedWritter.write("статичный");
            }
            if (!(Modifier.isStatic(field.getModifiers()))){
                bufferedWritter.write("объектный");
            }
    }
        bufferedWritter.newLine();
        bufferedWritter.write("\nметоды :\n");
        for (Method method : clazz.getDeclaredMethods()) {
            bufferedWritter.newLine();
            bufferedWritter.write("\n" + method.getName() + " возвращает ");
            if (method.getGenericReturnType().getTypeName().equals("void")){
                bufferedWritter.write("ничего, принимает " + getMethodParameters(method));
            }else {
                bufferedWritter.write(method.getGenericReturnType().getTypeName());
                bufferedWritter.write(", принимает " + getMethodParameters(method));
            }


        }
        bufferedWritter.close();
    }
    public String getMethodParameters(Method method){
        StringBuilder sb = new StringBuilder();
        for (Parameter p :method.getParameters() ) {
            sb.append(p.getParameterizedType()+" ");
        }
        return String.valueOf(sb);
    }

    public BufferedWriter returnBufferedWritter()  {
        try {
        File newFile = new File(filePath);

       if (newFile.createNewFile()) {
            System.out.println("Новый файл создан");
        } else {
            System.out.println("Файл уже существует");
        }
            bufferedWritter = new BufferedWriter(new FileWriter(newFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bufferedWritter;
    }

}


