package lesson7;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class BookParser extends Thread{

    private String pathName;

   BookParser(String pathName){
       this.pathName = pathName;
   }

    public  void fileParser() throws InterruptedException {

                Parcer.setLetters(0);
                Parcer.setNumbers(0);
                List<String> list = new Parcer(pathName, 0).scanFile();
                ExecutorService service = Executors.newFixedThreadPool(3);
                for (String s : list) {
                    Parcer p = new Parcer(s);
                    service.submit(p);
                }
                service.shutdown();
                this.sleep(5000);// знаю что так делать нежелательно,но не додумался как дождаться окончания потоков в пуллах
                System.out.println("Количество букв:" + Parcer.getLetters() + "\nКоличество цифр:" + Parcer.getNumbers());

    }

    @Override
    public void run() {
        try {
            fileParser();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
