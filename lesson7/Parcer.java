package lesson7;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Parcer extends Thread {

    private String pathName ;
    private BufferedReader bufferedReader;
    private String s;
    private static int letters = 0;
    private static int numbers = 0;

    public Parcer(String s) {
        this.s = s;
    }

    public Parcer(){};

    public Parcer(String pathName,int i){
        this.pathName = pathName;
        returnBufferedRieder(pathName);
    }

    public BufferedReader returnBufferedRieder(String s) {
        try {
            bufferedReader = new BufferedReader(new FileReader(s));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bufferedReader;
    }

    public List<String> scanFile() {
        int countOfLines = 0;
        String line;
        List<String> lines = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();

        try {
            while ((line = bufferedReader.readLine()) != null) {
                if (countOfLines == 50) {
                    lines.add(String.valueOf(stringBuilder));
                    countOfLines = 0;
                    stringBuilder.setLength(0);
                }
                stringBuilder.append(line);
                countOfLines++;

            }
            lines.add(String.valueOf(stringBuilder));
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public static void counter(String s) {
            synchronized (BookParser.class) {
                int amountOfChars = 0;
                int amountOfNumbers = 0;

                for (int i = 0; i < s.length(); i++) {
                    if ((int) s.charAt(i) >= 1040 && (int) s.charAt(i) <= 1103) {
                        amountOfChars++;
                    }
                    if ((int) s.charAt(i) >= 48 && (int) s.charAt(i) <= 57) {
                        amountOfNumbers++;
                    }
                }

                letters += amountOfChars;
                numbers += amountOfNumbers;
            }
    }

    @Override
    public void run() {
        counter(s);

    }

    public static int getLetters() {
        return letters;
    }

    public static void setLetters(int letters) {
        Parcer.letters = letters;
    }

    public static int getNumbers() {
        return numbers;
    }

    public static void setNumbers(int numbers) {
        Parcer.numbers = numbers;
    }
}
