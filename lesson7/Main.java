package lesson7;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class Main {

    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("введите путь к файлу (Например H:\\books\\source.txt)");
            BookParser bp = new BookParser(scanner.nextLine());
            bp.start();

        }
    }
}