package protskiy_nikita_generiks;


public class Main {
    public static void main(String[] args) {
        Zoo zoo = Zoo.getInstanse();
        zoo.putCellsIntoZoo();
        zoo.feedAnimals();
    }
}
