package protskiy_nikita_generiks;


public class Plants extends Food {

    public Plants(String name) {
        super(name);
        this.isVegeterian = true;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVegeterian() {
        return isVegeterian;
    }
}
