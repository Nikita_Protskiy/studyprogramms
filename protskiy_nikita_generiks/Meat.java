package protskiy_nikita_generiks;


public class Meat extends Food {
    private String name;
    private  boolean isVegeterian;


    public Meat(String name) {
        super(name);
        this.isVegeterian = false;
    }

    public String getName() {
        return name;
    }

    public boolean isVegeterian() {
        return isVegeterian;
    }

    public void setName(String name) {
        this.name = name;
    }
}
