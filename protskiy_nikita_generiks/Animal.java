package protskiy_nikita_generiks;


public abstract class Animal<T extends Food> {
    private String name;


    public Animal(String name) {
        this.name = name;
    }

    public abstract void eat(T food);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
