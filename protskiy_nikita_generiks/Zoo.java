package protskiy_nikita_generiks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Zoo {
    public static Zoo instanse = null;
    public static Zoo getInstanse(){
        if (instanse == null){
            instanse = new Zoo();
        }
        return instanse;
    }

        Animal<Food> lion = new Predator("Lion");
        Animal<Food> monkey = new Herbivores("Monkey");

        Cell<Animal> cellWithLions = new Cell<>(lion);
        Cell<Animal> cellWithMonkeys = new Cell<>(monkey);

        Food bananas = new Plants("бананы");
        Food meat = new Meat("мясо");

        List<Cell> cells = new ArrayList<>();
        public void putCellsIntoZoo(){
            cells.add(cellWithLions);
            cells.add(cellWithMonkeys);
        }

        public void feedAnimals(){
            for (Iterator<Cell> iterator = cells.iterator(); iterator.hasNext(); ) {
                Cell cell = iterator.next();
                cell.getAnimals().eat(bananas);
                cell.getAnimals().eat(meat);
        }




    }
}





