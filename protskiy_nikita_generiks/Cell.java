package protskiy_nikita_generiks;

public class Cell<V extends Animal> {
    private V animals;

    public Cell(V animals) {
        this.animals = animals;
    }

    public Cell() {
    }

    public V getAnimals() {
        return animals;
    }

    public void setAnimals(V animals) {
        this.animals = animals;
    }
}
