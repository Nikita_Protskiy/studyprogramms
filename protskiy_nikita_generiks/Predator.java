package protskiy_nikita_generiks;


public class Predator extends Animal {

    private Food food;

    public Predator(String name) {
        super(name);
    }

    public void eat(Food food) {
        setFood(food);
        if (food.isVegeterian == false) {
            System.out.println("Животное: " + this.getName()
                    + " съело " + this.getFood().name);
        }else {
            System.out.println("Животное: "+this.getName() +" не ест " + this.getFood().name);
        }
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food =  food;
    }
}
