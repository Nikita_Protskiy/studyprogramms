package data_base;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;


public class BookDao implements interfaseDAO<Book> {
    DBConnection dbConnection = DBConnection.getInstance();
    AuthorDAO authorDAO = new AuthorDAO();

    @Override
    public Collection<Book> getAll(String sql) {
        Collection<Book> res = new ArrayList<Book>();
        try {
            Statement statement = dbConnection.getConnection().createStatement();
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                Book book = new Book();
                book.setId(rs.getInt("id"));
                book.setName(rs.getString("name"));
                book.setAuthor(rs.getInt("author_id"));
                book.setAuthorName(authorDAO.getAuthorById(rs.getInt("author_id")));
                res.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public void executeInsert(Book book) {
        String s = "insert into Books(name,author_id) values( '" + book.getName() + "','" + book.getAuthor() + "')";
        dbConnection.executeUpdate(s);
        System.out.println("Книга добавлена");
    }

    @Override
    public void executeUpdate(Book book, int id) {
        String s = "update Books set name ='" + book.getName() + "'" +
                ",author_id =" + book.getAuthor() + " where id = " + id + ";";
        dbConnection.executeUpdate(s);
    }

    @Override
    public void executeDelete(int id) {
        String s = "delete from Books where id = " + id + ";";
        dbConnection.executeUpdate(s);
    }
}
