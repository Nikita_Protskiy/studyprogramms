package data_base;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;


public class DBConnection {
    private static DBConnection instance;
    private final static String URL_CONNECTION = "jdbc:sqlite:H:\\geekbrains\\java_2_db\\lesson2.db";
    private Connection connection;

    public Connection getConnection() {
        return connection;
    }

    public static DBConnection getInstance(){
        if(instance == null){
            instance = new DBConnection();
        }return instance;
    }

    private  DBConnection() {
        try {
            DriverManager.registerDriver(new org.sqlite.JDBC());
            this.connection = DriverManager.getConnection(URL_CONNECTION);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void executeUpdate(String sql){
        Statement statement = null;
        try {
            statement = this.connection.createStatement();
            statement.executeUpdate(sql);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public Collection<String> executeSelect(String sql) {
        Collection<String> res = new ArrayList<String>();
        try {
            Statement statement = this.connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            ResultSetMetaData md = rs.getMetaData();
            String[] columns = getColumns(md);
            StringBuilder sb = new StringBuilder();

            while(rs.next()){
                sb.setLength(0);
                for (String column : columns) {
                    sb.append(String.format("[%1$s:%2$s]\n", column ,rs.getObject(column)));
                }
                res.add(sb.toString());
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        return res;
    }
    public String[] getColumns(ResultSetMetaData md){
        String [] res = null;
        try {
            res = new String[md.getColumnCount()];
            for (int i = 0; i < res.length; i++) {
                res[i] = md.getColumnLabel(i+1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
}
