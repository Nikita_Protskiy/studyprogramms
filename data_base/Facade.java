package data_base;

import java.util.Scanner;


public class Facade {//консольный вариант

    DBConnection db = DBConnection.getInstance();
    AuthorDAO authorDAO = new AuthorDAO();
    BookDao bookDAO = new BookDao();
    Scanner sc = new Scanner(System.in);

    public void menu() {
        System.out.println("выберите пункт меню:\n1) - добавить автора" +
                "\n2) - обновить автора" +
                "\n3) - удалить автора" +
                "\n4) - показать список авторов" +
                "\n5) - добавить книгу" +
                "\n6) - обновить книгу" +
                "\n7) - удалить книгу" +
                "\n8) - показать список книг" +
                "\n9) - показать все книги автора" +
                "\n10) - выход ");
        int menu = Integer.parseInt(sc.nextLine());
        switch (menu){
            case 1:
                addAuthor();
                menu();
                break;
            case 2:
                modifideAuthor();
                menu();
                break;
            case 3:
                deleteAuthor();
                menu();
                break;
            case 4:
                selectAllAuthors();
                menu();
            case 5:
                addBook();
                menu();
                break;
            case 6:
                modifideBook();
                menu();
                break;
            case 7:
                deleteBook();
                menu();
                break;
            case 8:
                selectAllBooks();
                menu();
                break;
            case 9:
                getAllAuthorsBooks();
                menu();
                break;
            case 10:
                break;
        }
    }

    private void getAllAuthorsBooks() {
        selectAllAuthors();
        System.out.println("введите идентификатор автора:");
        int id_value = Integer.parseInt(sc.nextLine());
        String s ="select * from Books where author_id ="+id_value+";";
        System.out.println(db.executeSelect(s));
    }

    private void selectAllBooks() {
        System.out.println("Список всех книг");
        String s = "select * from Books";
        System.out.println(bookDAO.getAll(s));
    }

    private void deleteBook() {
        System.out.println(db.executeSelect("select * from Books;"));
        System.out.println("выберите индентификатор книги");
        int id = Integer.parseInt(sc.nextLine());
        System.out.println("вы точно хотите удалить книгу? - (Y/N)");
        String value = sc.nextLine();
        if (value.equals("y") || value.equals("Y")) {
            bookDAO.executeDelete(id);
            System.out.println("книга удалена");
        }else {
            System.out.println("книга не удалена");
        }
    }

    private void modifideBook() {
        Book book = new Book();
        System.out.println(db.executeSelect("select * from Books;"));
        System.out.println("введите id книги");
        int id = Integer.parseInt(sc.nextLine());
        System.out.println("введите новое название");
        String value = sc.nextLine();
        book.setName(value);
        System.out.println("введите id нового автора");
        int author_id = Integer.parseInt(sc.nextLine());
        book.setAuthor(author_id);
        bookDAO.executeUpdate(book,id);
    }

    private void addBook() {
        Book book = new Book();
        System.out.println("введите название книги:");
        String value = sc.nextLine();
        book.setName(value);
        System.out.println("введите идентификатор автора:");
        int id_value = Integer.parseInt(sc.nextLine());
        book.setAuthor(id_value);
        bookDAO.executeInsert(book);
    }

    private void selectAllAuthors() {
        System.out.println("Список всех авторов");
        String s = "select * from Author";
        System.out.println(authorDAO.getAll(s));
    }

    private void deleteAuthor() {
        System.out.println(db.executeSelect("select * from Author;"));
        System.out.println("выберите индентификатор автора");
        int id = Integer.parseInt(sc.nextLine());
        System.out.println("вы точно хотите удалить автора? - (Y/N)");
        String value = sc.nextLine();
        if (value.equals("y") || value.equals("Y")) {
            authorDAO.executeDelete(id);
            System.out.println("автор удален");
        }else {
            System.out.println("автор не удален");
        }
    }

    private void modifideAuthor() {
        Author author = new Author();
        System.out.println(db.executeSelect("select * from Author;"));
        System.out.println("введите id автора");
        int id = Integer.parseInt(sc.nextLine());
        System.out.println("введите новое имя");
        String value = sc.nextLine();
        author.setFirstName(value);
        System.out.println("введите новое отчество");
        value = sc.nextLine();
        author.setMiddleName(value);
        System.out.println("введите новую фамилию");
        value = sc.nextLine();
        author.setSecondName(value);
        authorDAO.executeUpdate(author,id);

    }

    private void addAuthor() {
        Author author = new Author();
        System.out.println("введите имя автора:");
        String value = sc.nextLine();
        author.setFirstName(value);
        System.out.println("введите отчество автора:");
        value = sc.nextLine();
        author.setMiddleName(value);
        System.out.println("введите фамилию автора:");
        value = sc.nextLine();
        author.setSecondName(value);
        authorDAO.executeInsert(author);
    }

}
