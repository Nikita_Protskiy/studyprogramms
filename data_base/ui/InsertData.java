package data_base.ui;

import data_base.Author;
import data_base.AuthorDAO;
import data_base.Book;
import data_base.BookDao;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class InsertData implements ActionListener {

    AuthorDAO authorDAO = new AuthorDAO();
    BookDao bookDao = new BookDao();
    JTextField textFieldFN, textFieldMN, textFieldSN, tfBookName, tfAuthorId, tfBookId;
    JLabel jLabelFN, jLabelMN, jLabelSN, jLabelBookName, jLabelAuthorId, jLabelBookId,jLabelMessage;
    JButton addAuthor, addBook, showAllAuthorsBooks, deleteAuthor, deleteBook,showAllAuthors;

    public InsertData() {

        JFrame insertFrame = new JFrame("Insert Data");
        insertFrame.setLayout(new FlowLayout());
        insertFrame.setSize(600, 300);
        Box box = Box.createVerticalBox();
        Box box2 = Box.createVerticalBox();
        Box box3 = Box.createVerticalBox();
        Box box4 = Box.createHorizontalBox();
        textFieldFN = new JTextField(10);
        textFieldFN.addActionListener(this);
        textFieldMN = new JTextField(10);
        textFieldMN.addActionListener(this);
        textFieldSN = new JTextField(10);
        textFieldSN.addActionListener(this);
        tfAuthorId = new JTextField(5);
        tfAuthorId.addActionListener(this);
        tfBookName = new JTextField(10);
        tfBookName.addActionListener(this);
        tfBookId = new JTextField(5);
        tfBookId.addActionListener(this);
        jLabelFN = new JLabel("First Name");
        jLabelMN = new JLabel("Middle Name");
        jLabelSN = new JLabel("Second Name");
        jLabelAuthorId = new JLabel("Author ID");
        jLabelBookId = new JLabel("Book ID");
        jLabelBookName = new JLabel("Name of Book");
        jLabelMessage = new JLabel();
        addAuthor = new JButton("Add Author");
        addBook = new JButton("Add Book");
        showAllAuthorsBooks = new JButton("Show all authors books");
        deleteAuthor = new JButton("Delete Author by ID");
        deleteBook = new JButton("Delete Book by ID");
        showAllAuthors = new JButton("Show all Authors");
        showAllAuthors.addActionListener(this);
        deleteAuthor.addActionListener(this);
        deleteBook.addActionListener(this);
        addAuthor.addActionListener(this);
        addBook.addActionListener(this);
        showAllAuthorsBooks.addActionListener(this);
        box.add(jLabelFN);
        box.add(textFieldFN);
        box.add(jLabelMN);
        box.add(textFieldMN);
        box.add(jLabelSN);
        box.add(textFieldSN);
        box.add(addAuthor);
        insertFrame.add(box);
        box2.add(jLabelBookName);
        box2.add(tfBookName);
        box2.add(jLabelAuthorId);
        box2.add(tfAuthorId);
        box2.add(addBook);
        insertFrame.add(box2);
        box3.add(jLabelBookId);
        box3.add(tfBookId);
        box3.add(deleteBook);
        insertFrame.add(box3);
        box4.add(deleteAuthor);
        box4.add(showAllAuthorsBooks);
        box4.add(showAllAuthors);
        insertFrame.add(box4);
        insertFrame.add(jLabelMessage);
        insertFrame.setDefaultCloseOperation(insertFrame.EXIT_ON_CLOSE);
        insertFrame.setVisible(true);
        insertFrame.setLocationRelativeTo(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String[] authorColumns = new String[]{"id", "firstName", "middleName", "secondName"};
        String[] bookColumns = new String[]{"id", "Name", "Author ID", "Author Name"};
        SelectFrame myFrame = new SelectFrame("DataBase", 500, 300, authorColumns, new SelectFrame.ObjectArrayInterface<Author>() {
            @Override
            public Object[] getObjects(Author author) {
                return new Object[]{author.getId(), author.getFirstName(), author.getMiddleName(), author.getSecondName()};
            }
        });
        SelectFrame myBookFrame = new SelectFrame("DataBase", 500, 300, bookColumns, new SelectFrame.ObjectArrayInterface<Book>() {
            @Override
            public Object[] getObjects(Book book) {
                return new Object[]{book.getId(), book.getName(), book.getAuthor(), book.getAuthorName()};
            }
        });
        if (e.getActionCommand().equals("Add Author")) {
            Author author = new Author();
            author.setFirstName(textFieldFN.getText());
            author.setMiddleName(textFieldMN.getText());
            author.setSecondName(textFieldSN.getText());
            authorDAO.executeInsert(author);
            myFrame.showFrame(authorDAO.getAll("select * from Author"));
        }
        if (e.getActionCommand().equals("Show all authors books")){
            if(tfAuthorId.getText().isEmpty()){
                jLabelMessage.setText("Insert please ID into the field: Author ID ");
            }else {
                String s = "select * from Books where author_id =" + Integer.parseInt(tfAuthorId.getText()) + ";";
                myBookFrame.showFrame(bookDao.getAll(s));
            }
        }
        if (e.getActionCommand().equals("Add Book")) {
            if(tfAuthorId.getText().isEmpty()){
                jLabelMessage.setText("Insert please ID into the field: Author ID ");
            }else {
                Book book = new Book();
                book.setName(tfBookName.getText());
                book.setAuthor(Integer.parseInt(tfAuthorId.getText()));
                bookDao.executeInsert(book);
                myBookFrame.showFrame(bookDao.getAll("select * from Books"));
            }
        }
        if(e.getActionCommand().equals("Delete Author by ID")){
            if(tfAuthorId.getText().isEmpty()){
                jLabelMessage.setText("Insert please ID into the field: Author ID ");
            }else {
                authorDAO.executeDelete(Integer.parseInt(tfAuthorId.getText()));
            }
        }
        if(e.getActionCommand().equals("Delete Book by ID")){
            if(tfBookId.getText().isEmpty()){
                jLabelMessage.setText("Insert please ID into the field: Book ID ");
            }else {
                bookDao.executeDelete(Integer.parseInt(tfBookId.getText()));
            }
        }
        if (e.getActionCommand().equals("Show all Authors")){
            myFrame.showFrame(authorDAO.getAll("select * from Author"));
        }
    }
}