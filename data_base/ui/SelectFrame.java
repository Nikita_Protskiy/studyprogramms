package data_base.ui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Collection;


public class SelectFrame<T> extends BaseFrame {

        private DefaultTableModel model;
        private JTable table;
        public static interface ObjectArrayInterface<T> {
            public Object[]getObjects(T t);
        }
        private ObjectArrayInterface objectArrayInterface;

    SelectFrame(String title, int width, int height,String[]column,ObjectArrayInterface<T> objectArrayInterface) {
        super(title, width, height);
        this.getContentPane().setLayout(new FlowLayout());
        this.table = new JTable();
        this.model = new DefaultTableModel(column,200);
        this.table.setModel(model);
        JScrollPane pane = new JScrollPane(this.table);
        this.getContentPane().add(pane);
        this.objectArrayInterface = objectArrayInterface;

    }
    public void showFrame(Collection<T> collection){
        this.model.setRowCount(0);//��������� ����� ������
        setVisible(true);
        for (T t : collection) {
            model.addRow(objectArrayInterface.getObjects(t));
        }

    }
}
