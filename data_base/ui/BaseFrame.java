package data_base.ui;

import javax.swing.*;


public class BaseFrame extends JFrame {
    BaseFrame(String title,int width,int height) {
        this.setTitle(title);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.setSize(width,height);
        this.setLocationRelativeTo(null);
    }
}
