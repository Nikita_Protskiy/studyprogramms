package data_base;

import java.util.Collection;

/**
 * Created by Никита on 27.03.2016.
 */
public interface interfaseDAO<T> {

    Collection<T> getAll(String sql);

    void executeInsert(T a);

    void executeUpdate(T a,int id);

    void executeDelete(int id);
}
