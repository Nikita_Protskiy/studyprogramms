package data_base;

/**
 * Created by Никита on 27.03.2016.
 */
public class Author {
    private  int id;
    private String firstName;
    private String secondName;
    private String middleName;

    public Author(int id){
        this.id = id;
    }

    public Author() {
    }

    @Override
    public String toString() {
        return "\nAuthor{\n" +
                "id=" + id +
                "\nfirstName= " + firstName +
                "\nmiddleName= " + middleName +
                "\nsecondName= " + secondName +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
}
