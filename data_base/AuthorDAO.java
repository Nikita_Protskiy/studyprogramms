package data_base;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Никита on 27.03.2016.
 */
public class AuthorDAO implements interfaseDAO<Author> {
    DBConnection dbConnection = DBConnection.getInstance();
    Connection connection = null;

    @Override
    public Collection<Author> getAll(String sql) {
        Collection<Author> res = new ArrayList<Author>();
        try {
            Statement statement = dbConnection.getConnection().createStatement();
            ResultSet rs = statement.executeQuery(sql);
            ResultSetMetaData md = rs.getMetaData();
            while(rs.next()){
                Author author = new Author();
                author.setId(rs.getInt("id"));
                author.setFirstName(rs.getString("firstName"));
                author.setMiddleName(rs.getString("middleName"));
                author.setSecondName(rs.getString("secondName"));
                res.add(author);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public void executeInsert(Author author) {
        String s = "insert into Author(firstName,middleName,secondName) values( '"+author.getFirstName()+"','"+author.getMiddleName()+"','"
               +author.getSecondName()+"' )";
        dbConnection.executeUpdate(s);
       // System.out.println("Автор добавлен");
    }

    @Override
    public void executeUpdate(Author author,int id) {
        String s = "update Author set firstName ='"+author.getFirstName()+"'" +
                ",middleName ='"+author.getMiddleName()+"',secondName =" +
                "'" + author.getSecondName()+"' where id ="+id+";";
        dbConnection.executeUpdate(s);
    }//UPDATE table_name SET column1 = ‘data1’, column2 = ‘data2’ WHERE column3 = ‘data3’;

    @Override
    public void executeDelete(int id) {
        String s = "delete from Author where id = "+ id+ ";";
        dbConnection.executeUpdate(s);
    }

    public String getAuthorById(int id) {
        String s = "select * from Author where id = "+ id+ ";";
        String res = null;
        try {
            Statement statement = dbConnection.getConnection().createStatement();
            ResultSet rs = statement.executeQuery(s);
            while(rs.next()){
                res = rs.getString("firstName") + rs.getString("middleName") +rs.getString("secondName");
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return res;
    }
}
